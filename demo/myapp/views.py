from django.shortcuts import render

# Create your views here.

from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt

from .forms import UploadFileForm

def home(request):
    return render(request, 'home.html')

@csrf_exempt
def model_form_upload(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            #form.save()
            return redirect('home')
    else:
        form = UploadFileForm()
    
    return render(request, 'upload.html', {'form': form})
